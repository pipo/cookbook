VER=5.6.1
PKGVER=${VER}
NAME=qt-everywhere-opensource-src
TARBALLS="$NAME-$VER.tar.xz"
URL="http://download.qt.io/archive/qt/${VER:0:3}/${VER}/single/$NAME-$VER.tar.xz"
DEPENDS="alsa-lib dbus glib gst-plugins-base harfbuzz icu libjpeg-turbo libmng libpng 
libtiff libxkbcommon mesalib mtdev nss openssl pcre sqlite unixODBC 
xcb-util-image xcb-util-keysyms xcb-util-renderutil xcb-util-wm xorg-libraries"
BUILD_DEPENDS="base cups gtk2 pkg-config python2"

do_patch() { :
}

do_configure() { :
./configure -prefix         /usr                 \
            -bindir         /usr/lib/qt5/bin     \
            -plugindir      /usr/lib/qt5/plugins \
            -importdir      /usr/lib/qt5/imports \
            -headerdir      /usr/include/qt5     \
            -datadir        /usr/share/qt5       \
            -sysconfdir     /etc/xdg             \
            -docdir         /usr/share/doc/qt5   \
            -examplesdir    /usr/share/doc/qt5/examples \
            -translationdir /usr/share/qt5/translations \
            -confirm-license   \
            -opensource        \
            -release           \
            -dbus-linked       \
            -openssl-linked    \
            -system-harfbuzz   \
            -system-sqlite     \
            -nomake examples   \
            -no-nis            \
            -no-rpath          \
            -optimized-qmake   \
            -skip qtwebengine
}

do_make() { :
make
}

do_check() { :
}

do_install() { :
make INSTALL_ROOT=$DESTDIR install &&
find $DESTDIR/lib/pkgconfig -name "*.pc" -exec perl -pi -e "s, -L$PWD/?\S+,,g" {} \;
find $DESTDIR/ -name qt_lib_bootstrap_private.pri \
   -exec sed -i -e "s:$PWD/qtbase:/usr/lib/:g" {} \; &&

find $DESTDIR/ -name \*.prl \
   -exec sed -i -e '/^QMAKE_PRL_BUILD_DIR/d' {} \;

QT5BINDIR=/usr/bin/qt5

install -v -dm755 $DESTDIR/usr/share/pixmaps/                  &&

install -v -Dm644 qttools/src/assistant/assistant/images/assistant-128.png \
                  $DESTDIR/usr/share/pixmaps/assistant-qt5.png &&

install -v -Dm644 qttools/src/designer/src/designer/images/designer.png \
                  $DESTDIR/usr/share/pixmaps/designer-qt5.png  &&

install -v -Dm644 qttools/src/linguist/linguist/images/icons/linguist-128-32.png \
                  $DESTDIR/usr/share/pixmaps/linguist-qt5.png  &&

install -v -Dm644 qttools/src/qdbus/qdbusviewer/images/qdbusviewer-128.png \
                  $DESTDIR/usr/share/pixmaps/qdbusviewer-qt5.png &&

install -dm755 $DESTDIR/usr/share/applications &&

cat > $DESTDIR/usr/share/applications/assistant-qt5.desktop << EOF
[Desktop Entry]
Name=Qt5 Assistant
Comment=Shows Qt5 documentation and examples
Exec=$QT5BINDIR/assistant
Icon=assistant-qt5.png
Terminal=false
Encoding=UTF-8
Type=Application
Categories=Qt;Development;Documentation;
EOF

cat > $DESTDIR/usr/share/applications/designer-qt5.desktop << EOF
[Desktop Entry]
Name=Qt5 Designer
GenericName=Interface Designer
Comment=Design GUIs for Qt5 applications
Exec=$QT5BINDIR/designer
Icon=designer-qt5.png
MimeType=application/x-designer;
Terminal=false
Encoding=UTF-8
Type=Application
Categories=Qt;Development;
EOF

cat > $DESTDIR/usr/share/applications/linguist-qt5.desktop << EOF
[Desktop Entry]
Name=Qt5 Linguist
Comment=Add translations to Qt5 applications
Exec=$QT5BINDIR/linguist
Icon=linguist-qt5.png
MimeType=text/vnd.trolltech.linguist;application/x-linguist;
Terminal=false
Encoding=UTF-8
Type=Application
Categories=Qt;Development;
EOF

cat > $DESTDIR/usr/share/applications/qdbusviewer-qt5.desktop << EOF
[Desktop Entry]
Name=Qt5 QDbusViewer
GenericName=D-Bus Debugger
Comment=Debug D-Bus applications
Exec=$QT5BINDIR/qdbusviewer
Icon=qdbusviewer-qt5.png
Terminal=false
Encoding=UTF-8
Type=Application
Categories=Qt;Development;Debugger;
EOF

for file in moc uic rcc qmake lconvert lrelease lupdate; do
  ln -sfrvn $QT5BINDIR/$file /usr/bin/$file-qt5
done
}

