The Cookbook
============

This is the cookbook of the Pipo Distribution Project. each directory here
contains the necessary recipe to build the package of the same name.

State of this repository
------------------------

Currently there are 3 kinds of recipes : new recipes, old recipes and older
recipes. We are working on updating the recipes. Meanwhile, we are also adding
new recipes.

Features and syntax elements
----------------------------

The recipe is a simple shell script that defines a few variables and functions.
The package manager takes care to call the functions on the right order, and
takes the content of the variables to initialize the build procedure.

Look in the gcc recipe to get a complete example of what the syntax is.

A few variables need explanations. First, the package manager will define some
variables:
 - src_dir is the directory you are in when entering any function in the recipe
 - DESTDIR is the directory you have to install the files in (usually /dest)
 - download_dir is the directory where the tarballs and optional patches are
   downloaded.

Then the recipe must define some variables
 - *NAME* not necessary, used only in the recipe.
 - *VER* not necessary, used only in the recipe.
 - *PKGVER* is the version the package will get once compiled. It is required by
   the package manager and must comply to some rules: it cannot contain a "-"
   (dash) nor a " " (space). Then it should always be incremented as the package
   manager will not install any version that seems older to it.
 - *TARBALLS* is required, and contains the list of tarballs found in the
   downloads directory. The first one will be extracted to some directory, and
   the others will be extracted inside the first directory found in the first
   archive.
 - *URL* is required, and contains the list of files you need to download before
   you can compile the package. Usually, those files appear also in TARBALLS.
 - *DEPENDS* is required, and is the list of packages this package depends on.
 - *BUILD_DEPENDS* is required, and is the list of packages this package needs
   in order to be built. Those packages are not considered a dependency of the
   package itself. You should always make "base" a build dependency of your
   packages.
 - *CONTAINS* not necessary, list of other packages this package will build.
   you must create a new function do_install_* for each package you declare in
   CONTAINS. The * is the name of the package where you replace any "-" with a
   "_".
 - *MAIN="false"*, not necessary, to disable building a package with the same
   name as the recipe directory. It will only build the packages contained in 
   CONTAINS.
 - *PATENT="maybe"* indicates that the package contains patented code, and that
   the recipe contains code that can disable the use of patented code. Use "yes"
   to indicate that patented code has not been disabled. Use #IF_NO_PATENT,
   #IF_PATENT and #END_PATENT blocks in the recipe to indicate where code shoud
   be enabled / disabled in order to build the patented / patent-free package.
