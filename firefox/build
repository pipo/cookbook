NAME="firefox"
VER=47.0
PKGVER=${VER}
TARBALLS="${NAME}-${VER}.source.tar.xz"
URL="https://ftp.mozilla.org/pub/mozilla.org/${NAME}/releases/${VER}/source/${NAME}-${VER}.source.tar.xz
http://www.linuxfromscratch.org/patches/blfs/svn/firefox-$VER-gtk320-1.patch
http://www.linuxfromscratch.org/patches/blfs/svn/firefox-$VER-system_graphite2_harfbuzz-1.patch"
DEPENDS="alsa-lib curl dbus-glib gconf graphite gst-libav gst-plugins-good gtk3
harfbuzz hunspell icu libevent libnotify libpng libvpx nspr nss openssl pixman
python2 sqlite startup-notification zlib"
BUILD_DEPENDS="autoconf-2.13 base pkg-config gtk2 unzip which xorg-libraries yasm zip"


do_patch() { :
cd $NAME-$VER
cat > mozconfig << "EOF"
# If you have a multicore machine, all cores will be used by default.
# If desired, you can reduce the number of cores used, e.g. to 1, by
# uncommenting the next line and setting a valid number of CPU cores.
#mk_add_options MOZ_MAKE_FLAGS="-j1"

# If you have installed DBus-Glib comment out this line:
#ac_add_options --disable-dbus

# If you have installed dbus-glib, and you have installed (or will install)
# wireless-tools, and you wish to use geolocation web services, comment out
# this line
ac_add_options --disable-necko-wifi

# GStreamer is necessary for H.264 video playback in HTML5 Video Player if
# FFmpeg is not found at runtime;
# to be enabled, also remember to set "media.gstreamer.enabled" to "true"
# in about:config. If you have GStreamer 1.x.y, comment out this line and
# uncomment the following one:
#ac_add_options --disable-gstreamer
ac_add_options --enable-gstreamer=1.0

# Uncomment this option if you wish to build with gtk+-2
#ac_add_options --enable-default-toolkit=cairo-gtk2

# Uncomment these lines if you have installed optional dependencies:
ac_add_options --enable-system-hunspell
ac_add_options --enable-startup-notification

# Comment out following option if you have PulseAudio installed
ac_add_options --disable-pulseaudio

# If you have installed GConf, comment out this line
#ac_add_options --disable-gconf

# Comment out following options if you have not installed
# recommended dependencies:
ac_add_options --enable-system-sqlite
ac_add_options --with-system-libevent
ac_add_options --with-system-libvpx
ac_add_options --with-system-nspr
ac_add_options --with-system-nss
ac_add_options --with-system-icu

# If you are going to apply the patch for system graphite
# and system harfbuzz, uncomment these lines:
ac_add_options --with-system-graphite2
ac_add_options --with-system-harfbuzz

# Stripping is now enabled by default.
# Uncomment these lines if you need to run a debugger:
#ac_add_options --disable-strip
#ac_add_options --disable-install-strip

# The BLFS editors recommend not changing anything below this line:
ac_add_options --prefix=/usr
ac_add_options --enable-application=browser

ac_add_options --disable-crashreporter
ac_add_options --disable-updater
ac_add_options --disable-tests

ac_add_options --enable-optimize

ac_add_options --enable-gio
ac_add_options --enable-official-branding
ac_add_options --enable-safe-browsing
ac_add_options --enable-url-classifier

# From firefox-40, using system cairo causes firefox to crash
# frequently when it is doing background rendering in a tab.
#ac_add_options --enable-system-cairo
ac_add_options --enable-system-ffi
ac_add_options --enable-system-pixman

ac_add_options --with-pthreads

ac_add_options --with-system-bz2
ac_add_options --with-system-jpeg
ac_add_options --with-system-png
ac_add_options --with-system-zlib

mk_add_options MOZ_OBJDIR=@TOPSRCDIR@/firefox-build-dir
EOF
patch -Np1 -i ${tar_dir}/firefox-$VER-gtk320-1.patch
patch -Np1 -i ${tar_dir}/firefox-$VER-system_graphite2_harfbuzz-1.patch
sed -e '/#include/i\
    print OUT "#define _GLIBCXX_INCLUDE_NEXT_C_HEADERS\\n"\;' \
    -i nsprpub/config/make-system-wrappers.pl

sed -e '/#include/a\
    print OUT "#undef _GLIBCXX_INCLUDE_NEXT_C_HEADERS\\n"\;' \
    -i nsprpub/config/make-system-wrappers.pl
}

do_configure() { :
}

do_make() { :
cd $NAME-$VER
CFLAGS+=" -fno-delete-null-pointer-checks -fno-lifetime-dse -fno-schedule-insns2" \
CXXFLAGS+=" -fno-delete-null-pointer-checks -fno-lifetime-dse -fno-schedule-insns2" \
SHELL=/bin/sh make -f client.mk
}

do_check() { :
}

do_install() { :
cd $NAME-$VER
SHELL=/bin/sh make -f client.mk DESTDIR=$DESTDIR install INSTALL_SDK= &&

chown -R 0:0 $DESTDIR/usr/lib/firefox-${VER} &&
mkdir -pv $DESTDIR/usr/lib/mozilla/plugins  &&
ln -sfv ../../mozilla/plugins $DESTDIR/usr/lib/firefox-${VER}/browser
}

